#include "data_logging.h"
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"
uint16_t logCount;
float voltagelog = 0.0f;


 
static osTimerId_t pendTimerID;
static osTimerAttr_t TimerAttr = 
{
	.name = "pend_Timer",
};

static void log_pendulum(void *argument);


static void log_pendulum(void *argument)
{
	UNUSED(argument);
	voltagelog = pendulum_read_voltage();
	printf("%f,%f\n", logCount*0.001, voltagelog);
	logCount = logCount + 5;
	if(logCount == 2000)
	{
		pend_logging_stop();
	}	
}

 void logging_init(void)
{
	pendTimerID = osTimerNew(log_pendulum, osTimerPeriodic, NULL, &TimerAttr);
/* TODO: Initialise timer for use with pendulum data logging */
}

void pend_logging_start(void)
{
	if(!osTimerIsRunning(pendTimerID))
	{
	logCount = 0;
	osTimerStart(pendTimerID, 5);
/* TODO: Reset the log counter */
/* TODO: Start data logging timer at 200Hz */
	}
}

void pend_logging_stop(void)
{
	if(osTimerIsRunning(pendTimerID))
	{
	osTimerStop(pendTimerID);
	}
/* TODO: Stop data logging timer */
}





