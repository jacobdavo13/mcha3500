#include "pendulum.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include <stdint.h>

static GPIO_InitTypeDef GPIO_InitStructure;
static ADC_HandleTypeDef hadc1;
static ADC_ChannelConfTypeDef sConfigADC1;

float pendulum_read_voltage(void)
{
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, 0xff);
	float ADC_ = 0.0;
	ADC_ = HAL_ADC_GetValue(&hadc1);
	HAL_ADC_Stop(&hadc1);
	float voltage = 0.0;
	voltage = (3.3f/4095.0f)*ADC_;
	return voltage;
	
	
}


void pendulum_init(void)
{
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_ADC1_CLK_ENABLE();
	    
		
		
        GPIO_InitStructure.Pin = GPIO_PIN_0;
        GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
        GPIO_InitStructure.Pull = GPIO_NOPULL;
        GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
        
		HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
		
		hadc1.Instance = ADC1;
        hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
        hadc1.Init.Resolution = ADC_RESOLUTION_12B;
        hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
        hadc1.Init.ContinuousConvMode = DISABLE;
        hadc1.Init.NbrOfConversion = 1;
		
		HAL_ADC_Init(&hadc1);
		
		sConfigADC1.Channel = ADC_CHANNEL_8;
        sConfigADC1.Rank = 1;
        sConfigADC1.SamplingTime = ADC_SAMPLETIME_480CYCLES;
        sConfigADC1.Offset = 0;
		
		HAL_ADC_ConfigChannel(&hadc1, &sConfigADC1);
}


